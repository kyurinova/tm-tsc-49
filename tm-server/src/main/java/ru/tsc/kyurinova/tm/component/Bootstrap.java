package ru.tsc.kyurinova.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.api.service.dto.*;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.service.*;
import ru.tsc.kyurinova.tm.service.dto.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Setter
@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ILogService logService = new LogService(connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService, logService);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService, logService);

    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService, logService);

    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService, logService, this);

    @NotNull
    private final IAdminDataService adminDataService = new AdminDataService(this);

    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, logService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this, adminDataService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(this);


    public void start(@Nullable final String[] args) {
        try {
            System.out.println("** WELCOME TO TASK MANAGER **");
            initPID();
            initEndpoint();
            logService.initJmsLogger();
            logService.info("** TASK-MANAGER SERVER STARTED **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));

        } catch (@NotNull final Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
        registry(adminDataEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void prepareShutdown() {
        logService.info("** TASK-MANAGER SERVER STOPPED **");
    }

}
