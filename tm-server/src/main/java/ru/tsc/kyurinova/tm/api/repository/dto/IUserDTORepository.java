package ru.tsc.kyurinova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTORepository {

    void add(
            @NotNull final UserDTO user
    );

    void update(@NotNull final UserDTO user);

    @Nullable
    UserDTO findByLogin(
            @NotNull final String login
    );

    @Nullable
    UserDTO findByEmail(
            @NotNull final String email
    );

    void removeById(
            @NotNull final String id
    );

    void removeByLogin(
            @NotNull final String login
    );

    void remove(
            @NotNull final UserDTO user
    );

    @Nullable
    List<UserDTO> findAll(
    );

    void clear(
    );

    @Nullable
    UserDTO findById(
            @NotNull final String id
    );

    @Nullable
    @NotNull
    UserDTO findByIndex(
            @NotNull final Integer index
    );

    void removeByIndex(
            @NotNull final Integer index
    );

    int getSize(
    );

}
