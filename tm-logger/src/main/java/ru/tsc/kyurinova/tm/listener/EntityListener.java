package ru.tsc.kyurinova.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class EntityListener implements MessageListener {

    private final LoggerService loggerService;

    public EntityListener(final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String yaml = textMessage.getText();
        loggerService.log(yaml);
    }

}
