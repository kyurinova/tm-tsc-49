package ru.tsc.kyurinova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.sun.istack.NotNull;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(@NotNull final String yaml) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(yaml, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @NotNull final byte[] bytes = yaml.getBytes();
        @NotNull final File file = new File(table);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }

}
